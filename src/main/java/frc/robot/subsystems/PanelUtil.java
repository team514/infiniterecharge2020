/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

/**
 * Add your docs here.
 */
public class PanelUtil extends SubsystemBase {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private final TalonSRX spinController;
  private final ColorSensorV3 colorSensor;
  private final Solenoid piston;
  private int countPasses;
  boolean deployed = false;

  private final ColorMatch colorMatch = new ColorMatch();

  public final Color blueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
  public final Color greenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
  public final Color redTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
  public final Color yellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);  
  public final Color whiteTarget = ColorMatch.makeColor(0.273, 0.500, 0.219);

  public PanelUtil() {
    I2C.Port i2cPort = I2C.Port.kOnboard;
    spinController = new TalonSRX(0);
    colorSensor = new ColorSensorV3(i2cPort);
    piston = new Solenoid(0);
    colorMatch.addColorMatch(blueTarget);
    colorMatch.addColorMatch(greenTarget);
    colorMatch.addColorMatch(redTarget);
    colorMatch.addColorMatch(yellowTarget);
    colorMatch.addColorMatch(whiteTarget);
  }

  public void setCountPasses(int passes) {
    this.countPasses = passes;
  } 

  public int getCountPasses() {
    return countPasses;
  }

  public void setSpinMotor(double speed) {
    spinController.set(ControlMode.Velocity, speed);
  }

  public Color getColor() {
    return colorSensor.getColor();
  }

  public Color getMatchedColor() {
    Color color = getColor();

    Color matchedColor;
    ColorMatchResult match = colorMatch.matchClosestColor(color);

    if (match.color == blueTarget) {
      matchedColor = blueTarget;
    } else if (match.color == redTarget) {
      matchedColor = redTarget;
    } else if (match.color == greenTarget) {
      matchedColor = greenTarget;
    } else if (match.color == yellowTarget) {
      matchedColor = yellowTarget;
    } else if (match.color == whiteTarget) {
      matchedColor = whiteTarget;
    } else {
      matchedColor = null;
    }
    return matchedColor;
  }
  
  public int getProximity() {
    return colorSensor.getProximity();
  }

  public void setPiston(boolean b){
    piston.set(b);
  }

  public void togglePanel(){
    if(deployed){
      piston.set(false);
      deployed = false;
    } else {
      piston.set(true);
      deployed = true;
    }
  }

  @Override
  public void periodic() {
    Color color = getColor();

    String colorString;
    ColorMatchResult match = colorMatch.matchClosestColor(color);

    if (match.color == blueTarget) {
      colorString = "Blue";
    } else if (match.color == redTarget) {
      colorString = "Red";
    } else if (match.color == greenTarget) {
      colorString = "Green";
    } else if (match.color == yellowTarget) {
      colorString = "Yellow";
    } else if (match.color == whiteTarget) {
      colorString = "White";
    } else {
      colorString = "Unknown";
    }

    SmartDashboard.putNumber("Red", color.red*255);
    SmartDashboard.putNumber("Green", color.green*255);
    SmartDashboard.putNumber("Blue", color.blue*255);
    SmartDashboard.putNumber("Confidence", match.confidence);
    SmartDashboard.putString("Detected Color", colorString);
    SmartDashboard.putNumber("Proximity", getProximity());
  }

}
