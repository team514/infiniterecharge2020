/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.mag;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CollectionUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.ShotUtil;

public class BeltControl extends CommandBase {
  private MagUtil magUtil;
  private CollectionUtil collectionUtil;
  private ShotUtil shotUtil;
  /**
   * Creates a new BeltControl.
   */
  public BeltControl(MagUtil magUtil, CollectionUtil collectionUtil, ShotUtil shotUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.magUtil = magUtil;
    this.collectionUtil = collectionUtil;
    this.shotUtil = shotUtil;
    addRequirements(magUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    boolean collectorDeployed = collectionUtil.getCollectorPiston();
    boolean fastEnough = shotUtil.isFastEnough();
    boolean shooting = shotUtil.isShooting();

    boolean runBelts = false;

    if (collectorDeployed || (shooting && fastEnough)) {
      // Run belts if:
      // Collector is deployed
      // OR we want to shoot and the motor is fast enough
      runBelts = true;
    }

    if (runBelts) {
      magUtil.runBelt(0.5);
    } else {
      magUtil.runBelt(0);
    }

  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
