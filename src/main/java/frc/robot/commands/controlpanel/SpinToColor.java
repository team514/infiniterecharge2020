/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.controlpanel;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.PanelUtil;

public class SpinToColor extends CommandBase {
  /**
   * Creates a new SpinToColor.
   */
  Color color;
  PanelUtil panelUtil;
  boolean done = false;

  public SpinToColor(PanelUtil panelUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.panelUtil = panelUtil;
    addRequirements(panelUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
    String gameData = DriverStation.getInstance().getGameSpecificMessage();
    if (gameData.length() > 0) {
      switch (gameData.charAt(0)) {
      case 'B':
        color = panelUtil.redTarget;
        break;
      case 'G':
        color = panelUtil.yellowTarget;
        break;
      case 'R':
        color = panelUtil.blueTarget;
        break;
      case 'Y':
        color = panelUtil.greenTarget;
        break;
      }
    } else {
      color = null;
    }
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    Color currentValue = panelUtil.getMatchedColor();
    if (currentValue == null) {
      done = true;
      return;
    }
    if (currentValue.equals(color)) {
      panelUtil.setSpinMotor(0.0);
      done = true;
    } else {
      panelUtil.setSpinMotor(Constants.spinMotorSpeed);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
