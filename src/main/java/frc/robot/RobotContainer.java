/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.commands.auto.groups.AutoLeaveLine;
import frc.robot.commands.auto.groups.AutoLeft;
import frc.robot.commands.auto.groups.AutoRight;
import frc.robot.commands.auto.groups.AutoStraight;
import frc.robot.commands.collection.ToggleCollector;
import frc.robot.commands.controller.OperatorController;
import frc.robot.commands.controlpanel.SpinColorWheel;
import frc.robot.commands.controlpanel.SpinToColor;
import frc.robot.commands.drive.CenterTarget;
import frc.robot.commands.drive.OperateDrive;
import frc.robot.commands.led.ControlLED;
import frc.robot.commands.mag.BeltControl;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.CollectionUtil;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.LEDUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.PanelUtil;
import frc.robot.subsystems.ShotUtil;
import frc.robot.subsystems.VisionUtil;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  //subsystems
  private final DriveUtil driveUtil = new DriveUtil();
  private final PanelUtil panelUtil = new PanelUtil();
  private final ClimbUtil climbUtil = new ClimbUtil();
  private final LEDUtil ledUtil = new LEDUtil();
  private final VisionUtil visionUtil = new VisionUtil();
  private final CollectionUtil collectionUtil = new CollectionUtil();
  private final MagUtil magUtil = new MagUtil();
  private final ShotUtil shotUtil = new ShotUtil();

  //default commands
  private final OperateDrive operateDrive = new OperateDrive(driveUtil);
  private final ControlLED controlLED = new ControlLED(ledUtil, visionUtil);
  private final BeltControl beltControl = new BeltControl(magUtil, collectionUtil, shotUtil);
  public final OperatorController operatorController = new OperatorController(visionUtil, climbUtil, collectionUtil, shotUtil);
  Joystick driver, right;
  XboxController operator;
  //driver
  JoystickButton changeDriveMode;
  //operator
  JoystickButton spinToColor, spinColorWheel, runCollector, resetGyro, toggleClimb, toggleCollector, togglePanel, toggleTension, centerTarget, shoot, toggleShot;
  //debug
  JoystickButton climbOpen, climbClose, spinCW, spinCCW;

  //Autonomous Stuffs...
  private final SendableChooser<String> chooser;

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    driver = new Joystick(0);
    right = new Joystick(1);
    operator = new XboxController(Constants.kOperatorPortNum);
    //Driver
    changeDriveMode = new JoystickButton(right, Constants.kXButtonNum);
    //Operator
    centerTarget = new JoystickButton(operator, Constants.kXInputXButtonNum);
    spinToColor = new JoystickButton(operator, Constants.kXInputBButtonNum);
    spinColorWheel = new JoystickButton(operator, Constants.kXInputAButtonNum);
    toggleCollector = new JoystickButton(operator, Constants.kXInputLeftBumperNum);
    toggleClimb = new JoystickButton(operator, Constants.kXInputStartButtonNum);
    togglePanel = new JoystickButton(operator, Constants.kXInputBackButtonNum);
    toggleTension = new JoystickButton(operator, Constants.kXInputYButtonNum);
    toggleShot = new JoystickButton(operator, Constants.kXInputRightBumperNum);
    resetGyro = new JoystickButton(operator, Constants.kRightStickButtonNum);

    //Debug
    climbOpen = new JoystickButton(right, Constants.kJoystickButton2);
    climbClose = new JoystickButton(right, Constants.kJoystickButton3);
    spinCW = new JoystickButton(operator, Constants.kAButtonNum);
    spinCCW = new JoystickButton(operator, Constants.kBButtonNum);

    // Instantiate Auto Stuffs...
    chooser = new SendableChooser<String>();

    // Configure the button bindings
    configureButtonBindings();
    setDefaultCommands();
    visionUtil.setCamDrive();
    visionUtil.setLEDOff();

  }

  private void setDefaultCommands() {
    ledUtil.setDefaultCommand(controlLED);
    driveUtil.setDefaultCommand(operateDrive);
    panelUtil.setDefaultCommand(new RunCommand(() -> {
      panelUtil.setSpinMotor(0);
    }, panelUtil));
    collectionUtil.setDefaultCommand(new RunCommand(() -> {
      if (collectionUtil.getCollectorPiston()){
        collectionUtil.setCollectorMotor(Constants.collectorSpeed);
      } else {
        collectionUtil.setCollectorMotor(0.0);
      }
    }, collectionUtil));
    magUtil.setDefaultCommand(beltControl);
    shotUtil.setDefaultCommand(new RunCommand(() -> {
      if (shotUtil.isRunning()) {
        shotUtil.setSpeed(.75);
        operator.setRumble(RumbleType.kLeftRumble, 1);
        operator.setRumble(RumbleType.kRightRumble, 1);
      } else {
        shotUtil.setSpeed(0);
      }
    }, shotUtil));
  }

  public void setChooser() {
    chooser.setDefaultOption("Leave Line", "Leave_Line");
    chooser.addOption("Right Side", "Right");
    chooser.addOption("Left Side", "Left");
    chooser.addOption("Center", "Straight");
    SmartDashboard.putData("Auto Mode : ", chooser);
  }

  public void calibrateGyro(){
    driveUtil.calibrateGyro();
  }
  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    
    //Driver
    changeDriveMode.whenPressed(new InstantCommand(() -> {
      driveUtil.setDriveMode(!driveUtil.getDriveMode());
    }, driveUtil));
  
    //Operator
    resetGyro.whenPressed(new InstantCommand(() -> {
      driveUtil.resetGyro();
    }, driveUtil));
;    centerTarget.whenPressed(new CenterTarget(driveUtil, visionUtil));
    spinToColor.whenPressed(new SpinToColor(panelUtil));
    spinColorWheel.whenPressed(new SpinColorWheel(panelUtil));
    toggleCollector.whenPressed(new ToggleCollector(collectionUtil));
    toggleTension.whenPressed(new InstantCommand(() -> {
      boolean currentTension = magUtil.hasTension();
      if (currentTension){
        magUtil.retractPiston();
      } else {
        magUtil.deployPiston();
      }
    }, magUtil));
    toggleShot.whenPressed(new InstantCommand(() -> {
      shotUtil.setRunning(!shotUtil.isRunning());
    }, shotUtil));
    toggleClimb.whenPressed(new InstantCommand(() -> {
      if (DriverStation.getInstance().getMatchTime() > 30) return;
      climbUtil.toggleClimb();
    }, climbUtil));
    togglePanel.whenPressed(new InstantCommand(() -> {
      panelUtil.togglePanel();
    }, panelUtil));
    toggleCollector.whenPressed(new InstantCommand(() -> {
      if (collectionUtil.getCollectorPiston()) {
        collectionUtil.retractPiston();
      } else {
        collectionUtil.deployPiston();
      }
    }, collectionUtil));

    //Debug
    climbOpen.whenPressed(new InstantCommand(climbUtil::open, climbUtil));
    climbClose.whenPressed(new InstantCommand(climbUtil::close, climbUtil));
    spinCW.whileHeld(new RunCommand(() -> {
      panelUtil.setSpinMotor(.5);
    }, panelUtil));
    spinCCW.whileHeld(new RunCommand(() -> {
      panelUtil.setSpinMotor(-.5);
    }, panelUtil));
  }

  public double getDriverLeftX(){
    return driver.getX(Hand.kLeft);
  }

  public double getDriverLeftY(){
    return driver.getY(Hand.kLeft);
  }

  public double getDriverRightX(){
    return driver.getX(Hand.kRight);
  }

  public double getDriverRightY(){
    return driver.getY(Hand.kRight);
  }

  public double getOperatorPOV(){
    return operator.getPOV();
  }

  public double getOperatorLeftTrigger(){
    return operator.getTriggerAxis(Hand.kLeft);
  }

  public double getOperatorRightTrigger(){
    return operator.getTriggerAxis(Hand.kRight);
  }

  public double getOperatorLeftX(){
    return operator.getX(Hand.kLeft);
  }

  public double getOperatorLeftY(){
    return operator.getY(Hand.kLeft);
  }

  public double getOperatorRightX(){
    return operator.getX(Hand.kRight);
  }

  public double getOperatorRightY(){
    return operator.getY(Hand.kRight);
  }
  

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    Command auto;
    String autoMode = chooser.getSelected();
    if (autoMode.equals("Left")) {
      auto = new AutoLeft(driveUtil, visionUtil, shotUtil, magUtil);
    } else if (autoMode.equals("Right")) {
      auto = new AutoRight(driveUtil, visionUtil, shotUtil, magUtil);
    } else if (autoMode.equals("Straight")) {
      auto = new AutoStraight(driveUtil, visionUtil, shotUtil, magUtil);
    } else {
      auto = new AutoLeaveLine(driveUtil);
    }
    return auto;
  }
  
}
