/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.controller;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbUtil;
import frc.robot.subsystems.CollectionUtil;
import frc.robot.subsystems.ShotUtil;
import frc.robot.subsystems.VisionUtil;
import frc.robot.Robot;

public class OperatorController extends CommandBase {
  /**
   * Creates a new DpadControl.
    */
  VisionUtil visionUtil;
  ClimbUtil climbUtil;
  CollectionUtil collectionUtil;
  ShotUtil shotUtil;
  boolean leftTrigger = false;
  boolean rightTrigger = false;

  public OperatorController(VisionUtil visionUtil, ClimbUtil climbUtil, CollectionUtil collectionUtil, ShotUtil shotUtil) {
    this.visionUtil = visionUtil;
    this.climbUtil = climbUtil;
    this.collectionUtil = collectionUtil;
    this.shotUtil = shotUtil;
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(Robot.robotContainer.getOperatorPOV() == 180){
      visionUtil.setPipelineLong();
    } else if(Robot.robotContainer.getOperatorPOV() == 0){
      visionUtil.setPipelineShort();
    } else if(Robot.robotContainer.getOperatorPOV() == 270){
      visionUtil.setCamDrive();
      visionUtil.setLEDOff();
    } else if(Robot.robotContainer.getOperatorPOV() == 90){
      visionUtil.setCamVision();
      visionUtil.setLEDOn();
    }

    if(Robot.robotContainer.getOperatorLeftTrigger() >= 0.5 && !leftTrigger){
      leftTrigger = true; //when pressed
    } else if (Robot.robotContainer.getOperatorLeftTrigger() < 0.5 && leftTrigger){
      leftTrigger = false; //when released
    }
    // if(Robot.robotContainer.getOperatorRightTrigger() >= 0.5 && !rightTrigger){
    //   rightTrigger = true; //when pressed
    //   shotUtil.setShooting(!shotUtil.isShooting());
    // } else if (Robot.robotContainer.getOperatorRightTrigger() < 0.5 && rightTrigger){
    //   rightTrigger = false; //when released
    // }
    
    shotUtil.setShooting(Robot.robotContainer.getOperatorRightTrigger() >= 0.5);

  }
  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
