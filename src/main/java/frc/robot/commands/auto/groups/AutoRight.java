/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.auto.groups;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.auto.AutoDriveStraight;
import frc.robot.commands.auto.AutoShot;
import frc.robot.commands.auto.AutoTurn;
import frc.robot.commands.drive.CenterTarget;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.ShotUtil;
import frc.robot.subsystems.VisionUtil;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class AutoRight extends SequentialCommandGroup {
  /**
   * Creates a new AutoRight.
   */
  public AutoRight(DriveUtil driveUtil, VisionUtil visionUtil, ShotUtil shotUtil, MagUtil magUtil) {
    // Add your commands in the super() call, e.g.
    // super(new FooCommand(), new BarCommand());
    addCommands(
      new InstantCommand(() -> {
        shotUtil.setSpeed(Constants.autoShotMotorSpeed);
      },shotUtil),
      new AutoDriveStraight(driveUtil, Constants.tickFoot*5, Constants.autoDriveSpeed),
      new AutoTurn(driveUtil, visionUtil, true),
      new CenterTarget(driveUtil, visionUtil),
      new AutoShot(shotUtil, magUtil)
    );
  }
}
