/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.collection;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CollectionUtil;

public class ToggleCollector extends CommandBase {
  /**
   * Creates a new ToggleCollector.speed
   */
  CollectionUtil collectionUtil;
  boolean done = false;
  public ToggleCollector(CollectionUtil collectionUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.collectionUtil = collectionUtil;
    addRequirements(collectionUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    collectionUtil.setCollectorPiston(!collectionUtil.getCollectorPiston());
    done = true;
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
