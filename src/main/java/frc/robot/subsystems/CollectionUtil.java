/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class CollectionUtil extends SubsystemBase {
  private final DoubleSolenoid piston;
  private final WPI_VictorSPX collector;
  boolean deployed = false;

  /**
   * Creates a new CollectionUtil.
   */

  public CollectionUtil() {
    piston = new DoubleSolenoid(Constants.collectionPistonA, Constants.collectionPistonB);
    collector = new WPI_VictorSPX(Constants.collector);
  }

  public void setCollectorMotor(double speed) {
    collector.set(speed);
  }

  public void setCollectorPiston(boolean b) {
    piston.set(b ? DoubleSolenoid.Value.kForward : DoubleSolenoid.Value.kReverse);
  }

  public void deployPiston() {
    piston.set(DoubleSolenoid.Value.kForward);
  }

  public void retractPiston() {
    piston.set(DoubleSolenoid.Value.kReverse);
  }

  public boolean getCollectorPiston() {
    return piston.get().equals(DoubleSolenoid.Value.kForward);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
