/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class MagUtil extends SubsystemBase {
  private final WPI_VictorSPX belt;
  private final Solenoid piston;
  private final DigitalInput bannerSensor;
  /**
   * Creates a new MagUtil.
   */
  public MagUtil() {
    belt = new WPI_VictorSPX(Constants.belt);
    piston = new Solenoid(Constants.beltPiston);
    bannerSensor = new DigitalInput(Constants.bannerSensor);
  }

  public void runBelt(double speed) {
    belt.set(speed);
  }

  public void deployPiston() {
    piston.set(true);
  }

  public void retractPiston() {
    piston.set(false);
  }

  public boolean isMagFilled() {
    return bannerSensor.get();
  }

  public boolean hasTension() {
    return piston.get();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
