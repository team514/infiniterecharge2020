/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveUtil extends SubsystemBase {
  WPI_VictorSPX leftMaster, leftSlave, rightMaster, rightSlave;
  boolean tankMode = true;
  double leftSpeed, rightSpeed, d_left, d_right, adjustment, speed;
  ADXRS450_Gyro gyro;
  Encoder leftEncoder, rightEncoder;
  /**
   * Creates a new DriveUtil.
   */
  public DriveUtil() {
    leftMaster = new WPI_VictorSPX(Constants.leftMaster);
    leftSlave = new WPI_VictorSPX(Constants.leftSlave);
    rightMaster = new WPI_VictorSPX(Constants.rightMaster);
    rightSlave = new WPI_VictorSPX(Constants.rightSlave);

    leftSlave.follow(leftMaster);
    rightSlave.follow(rightMaster);

    leftMaster.setInverted(true);
    leftSlave.setInverted(true);
    
    leftEncoder = new Encoder(Constants.leftEncoderForward, Constants.leftEncoderReverse);
    rightEncoder = new Encoder(Constants.rightEncoderForward, Constants.rightEncoderReverse);
  
    gyro = new ADXRS450_Gyro();
  }

  public void stopDrive(){
    driveTank(0.0, 0.0);
    resetEncoders();
    resetGyro();
  }

  public void resetGyro(){
    gyro.reset();
  }

  public void calibrateGyro(){
    gyro.calibrate();
  }

  public double getGyroAngle(){
    return gyro.getAngle();
  }

  public void resetEncoders() {
    leftEncoder.reset();
    rightEncoder.reset();
  }

  public double getLeftEncoder() {
    return Math.abs(leftEncoder.get());
  }

  public double getRightEncoder() {
    return Math.abs(rightEncoder.get());
  }

  public void driveTank(double left, double right) {
    left = squareInputs(left);
		right = squareInputs(right);
    leftMaster.set(ControlMode.PercentOutput, left);
    rightMaster.set(ControlMode.PercentOutput, right);
    leftSpeed = left;
    rightSpeed = right;
  }

  public void driveArcade(double xAxis, double yAxis) {
    double left, right;
    left = yAxis - xAxis;
    right = yAxis + xAxis;
    driveTank(left, right);
  }

  public boolean getDriveMode() {
    return tankMode;
  }

  public void setDriveMode(boolean driveMode) {
    tankMode = driveMode;
  }

  private double squareInputs(double d){
		boolean negative = false;
		
		if(d < 0){
			negative = true;
		}
		
		d = Math.pow(d, 2);
		
		if(negative){
			d = -d;
		}
		
		return d;
  }

  // public void autoStraight(double speed, double horizontalOffset) {
  //   driveAuto(-speed, horizontalOffset,
  //                   Constants.C2R_inputMin,
  //                   Constants.C2R_gyroInputMax);

  // }

  // // public void driveAuto(double driveSpeed, double setPoint, double min, double max){
	// // 	double adj = coerce2Range(setPoint, min, max);
	// // 	//left = left + adj;
	// // 	//right = right + -adj;
	// // 		this.adjustment = adj;
		
	// // 	d_right = driveSpeed + this.adjustment;
	// // 	d_left = driveSpeed - this.adjustment;
	// // 	driveTank(d_left, d_right);
	// // }
	
	public double coerce2Range(double input){
    // TO.DO code application logic here
    double inputMin, inputMax, inputCenter;
    double outputMin, outputMax, outputCenter;
    double scale, result;
    //double output;
    
    inputMin = Constants.C2R_inputMin; 
    inputMax = Constants.C2R_inputMax;
    
    outputMin = Constants.C2R_outputMin;
    outputMax = Constants.C2R_outputMax;
    
    // Determine the center of the input range and output range
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    // Scale the input range to the output range
    scale = (outputMax - outputMin) / (inputMax - inputMin);
    // Apply the transformation 
    result = (input + -inputCenter) * scale + outputCenter;
    // Constrain to the output range 
    speed = Math.max(Math.min(result, outputMax), outputMin);

    return speed;
  }


  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("Left Motor", -leftSpeed*100);
    SmartDashboard.putNumber("Right Motor", -rightSpeed*100);
    SmartDashboard.putNumber("Time", System.currentTimeMillis());
    SmartDashboard.putNumber("Gyro Angle", getGyroAngle());
  }
}
