/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveUtil;

public class AutoDriveStraight extends CommandBase {
  /**
   * Creates a new AutoDrive.
   */
  private DriveUtil driveUtil;
  private double ticks;
  private double speed;

  public AutoDriveStraight(DriveUtil driveUtil, double ticks, double speed) {
    // Use addRequirements() here to declare subsystem dependencies.
    
    this.driveUtil = driveUtil;
    this.ticks = ticks;
    this.speed = speed;
    
    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    driveUtil.resetEncoders();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    driveUtil.driveTank(speed, speed);
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return driveUtil.getLeftEncoder() >= ticks;
  }
}
