/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class AutoTurn extends CommandBase {

  private DriveUtil driveUtil;
  private VisionUtil visionUtil;
  boolean left;
  boolean done = false;
  /**
   * Creates a new AutoTurn.
   */
  public AutoTurn(DriveUtil driveUtil, VisionUtil visionUtil, boolean left) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;
    this.left = left;

    addRequirements(driveUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(visionUtil.hasTarget()){
      done = true;
    }else if(left){
      driveUtil.driveTank(-Constants.autoTurnSpeed, Constants.autoTurnSpeed);
    }else{
      driveUtil.driveTank(Constants.autoTurnSpeed, -Constants.autoTurnSpeed);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
