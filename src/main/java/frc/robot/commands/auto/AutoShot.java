/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.auto;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.MagUtil;
import frc.robot.subsystems.ShotUtil;

public class AutoShot extends CommandBase {
  /**
   * Creates a new AutoShot.
   */
  private ShotUtil shotUtil;
  private MagUtil magUtil;
  private boolean done;
  public AutoShot(ShotUtil shotUtil, MagUtil magUtil) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.shotUtil = shotUtil;
    this.magUtil = magUtil;
    addRequirements(shotUtil, magUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (magUtil.isMagFilled()) {
      shotUtil.setShooting(true);
      magUtil.deployPiston();
    }else{
      shotUtil.setShooting(false);
      magUtil.retractPiston();
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
