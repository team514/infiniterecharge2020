/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveUtil;
import frc.robot.subsystems.VisionUtil;

public class CenterTarget extends CommandBase {

  DriveUtil driveUtil;
  VisionUtil visionUtil;
  boolean done = false;
  int state;
  int countAttempts;
  double gyroAim;
  double gyroCurrent;
  boolean left;
  double speed;
  double originalTime;

  /**
   *
   * Creates a new CenterTarget.
   */

  public CenterTarget(DriveUtil driveUtil, VisionUtil visionUtil) {
    this.driveUtil = driveUtil;
    this.visionUtil = visionUtil;
    addRequirements(driveUtil);

    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    done = false;
    state = 0;
    countAttempts = 0;
    left = false;
    speed = Constants.autoTurnSpeed;
    originalTime = Timer.getFPGATimestamp();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (!visionUtil.hasTarget()) {
      done = true;
      return;
    }
    double horizontalOffset = visionUtil.getHorizontalOffset();
    if (Timer.getFPGATimestamp() - originalTime > 7) {
      done = true;
    } else if (horizontalOffset > .5) {
      driveUtil.driveTank(-Constants.autoTurnSpeed, Constants.autoTurnSpeed);
    } else if (horizontalOffset < -.5) {
      driveUtil.driveTank(Constants.autoTurnSpeed, -Constants.autoTurnSpeed);
    } else {
      done = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return done;
  }
}
