/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class VisionUtil extends SubsystemBase {
  /**
   * Creates a new VisionUtil.
   */
  NetworkTable table;
  NetworkTableEntry targetArea, pipelineLatency, skewRotation, validTarget, horizontalOffset, verticalOffset, ledMode, camMode, pipeline;
  double x, area, degrees, d;
  boolean lights, cameraMode;
  public boolean tracking, hasTarget;
  public VisionUtil() {

    table = NetworkTableInstance.getDefault().getTable("limelight");
    horizontalOffset = table.getEntry("tx");
    verticalOffset = table.getEntry("ty");
    validTarget = table.getEntry("tv");
    skewRotation = table.getEntry("ts");
    targetArea = table.getEntry("ta");
    pipelineLatency = table.getEntry("tl");
    ledMode = table.getEntry("ledMode");
    camMode = table.getEntry("camMode");
    pipeline = table.getEntry("pipeline");
    
    tracking = false;
    lights = false;
    hasTarget = false;
    setLEDOff();

  }

  public boolean getCamMode(){ //True when in tracking mode, false when in drive mode.
    boolean camMode = false;
    if(this.camMode.getDouble(0) == 0){
      camMode = true;
    } else if (this.camMode.getDouble(0) == 1){
      camMode = false;
    }
    return camMode;
  }

  public boolean hasTarget(){
    boolean hasTarget = false;
    if(this.validTarget.getDouble(0) == 0){
      hasTarget = false;
    } else if (this.validTarget.getDouble(0) == 1) {
      hasTarget = true;
    }
    return hasTarget;
  }

  public void setCamVision(){
    camMode.setNumber(0);  
  }

  public void setCamDrive(){
    camMode.setNumber(1); 
  }

  public double getLEDMode(){
    return camMode.getDouble(0);
  }

  public void setLEDOff(){
    ledMode.setNumber(1);
  }

  public void setLEDOn(){
    ledMode.setNumber(0);
  }

  public double getPipeline(){
    return pipeline.getDouble(0);
  }

  public void setPipelineLong(){
    pipeline.setNumber(0);
  }
  
  public void setPipelineShort(){
    pipeline.setNumber(1);
  }

  public void updateTargetData(){
    this.x = horizontalOffset.getDouble(0.0);
    this.area = targetArea.getDouble(0.0);
    this.hasTarget = (validTarget.getNumber(0).doubleValue() == 1.0);
    this.cameraMode = (camMode.getNumber(0).doubleValue() == 1.0);
  }

  public void toggleLights(boolean lights){
    if(lights){
      setLEDOn();
      setCamVision();
    }else{
      setLEDOff();
      setCamDrive();
    }
  }
  
  public double getHorizontalOffset(){
    NetworkTableEntry tx = table.getEntry("tx");
    return tx.getDouble(0);
  }

  public double getTargetArea(){
    NetworkTableEntry ta = table.getEntry("ta");
    return ta.getDouble(0);
  }

  public double calcDistance() {
    this.d = 0.0;
    this.d = (5.9957 * (Math.pow(Math.E, (-0.04*getTargetArea()))));
    return this.d;
  }

  @Override
  public void periodic() {
    SmartDashboard.putNumber("Horizontal Offset (Degrees)", horizontalOffset.getDouble(0.0));
    SmartDashboard.putNumber("Vertical Offset (Degrees)", verticalOffset.getDouble(0.0));
    SmartDashboard.putNumber("Skew/Rotation (Degrees)", skewRotation.getDouble(0.0));
    SmartDashboard.putNumber("Target Area (Percent)", targetArea.getDouble(0.0));
    SmartDashboard.putNumber("Pipeline Latency (ms)", pipelineLatency.getDouble(0.0));
    SmartDashboard.putBoolean("Camera Mode", this.cameraMode);
    SmartDashboard.putBoolean("Valid Target", this.hasTarget);
    SmartDashboard.putNumber("Adj. X Offset", horizontalOffset.getDouble(0.0)*1.06);
    // .add("Has Target", validTarget).withWidget("Boolean Box").withProperties("colorWhenTrue", "green", "colorWhenFalse", "red");
    // This method will be called once per scheduler run
  }
}
