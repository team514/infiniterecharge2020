/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.led;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LEDUtil;
import frc.robot.subsystems.LEDUtil.LEDColor;
import frc.robot.subsystems.LEDUtil.LEDStrip;
import frc.robot.subsystems.VisionUtil;

public class ControlLED extends CommandBase {
  /**
   * Creates a new AllianceColor.
   */
  LEDUtil ledUtil;
  VisionUtil visionUtil;
  double time;
  boolean blinkState;

  public ControlLED(LEDUtil ledUtil, VisionUtil visionUtil) {
    this.ledUtil = ledUtil;
    this.visionUtil = visionUtil;
    addRequirements(ledUtil);
    // Use addRequirements() here to declare subsystem dependencies.
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    time = Timer.getFPGATimestamp();
    blinkState = true;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (visionUtil.getCamMode()) { // If camera is on
      if (visionUtil.hasTarget()){ // If it sees a target.
        ledUtil.setLED(LEDStrip.ALL, LEDColor.GREEN); // Make the entire strip blink green.
      } else if (!visionUtil.hasTarget()){ // If it doesn't see a target.
        if (Timer.getFPGATimestamp() - time >= .5) {
          blinkState = !blinkState;
          time = Timer.getFPGATimestamp();
        }
        if (blinkState) {
          ledUtil.setLED(LEDStrip.ALL, LEDColor.GREEN);
        } else {
          ledUtil.setLED(LEDStrip.ALL, LEDColor.OFF);
        }
      } 
    } else {
      ledUtil.setAllianceMode();
    }


  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
