/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ShotUtil extends SubsystemBase {
  
  CANSparkMax shotMotor1, shotMotor2;
  CANEncoder encoder;
  boolean running = false;
  boolean shooting = false;
  double targetRPM = 0;

  /**
   * Creates a new ShotUtil.
   */
  public ShotUtil() {
    shotMotor1 = new CANSparkMax(Constants.shotMotor1, CANSparkMaxLowLevel.MotorType.kBrushless);
    shotMotor2 = new CANSparkMax(Constants.shotMotor2, CANSparkMaxLowLevel.MotorType.kBrushless);
    encoder = shotMotor1.getEncoder();
  }

  public void setRunning(boolean b) {
    running = b;
  }

  public void setShooting(boolean b) {
    shooting = b;
  }

  public double getRPM() {
    return encoder.getVelocity();
  }

  public boolean isShooting() {
    return shooting;
  }

  public boolean isRunning() {
    return running;
  }

  public boolean isFastEnough() {
    return getRPM() >= targetRPM;
  }

  public void setSpeed(double speed) {
    shotMotor1.set(speed);
  }
  
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
