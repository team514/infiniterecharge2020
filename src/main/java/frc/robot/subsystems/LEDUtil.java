/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class LEDUtil extends SubsystemBase {
  /**
   * Creates a new LEDUtil.
   */
  AddressableLED led;
  AddressableLEDBuffer ledBuffer;
  DriverStation driverStation = DriverStation.getInstance();

  public LEDUtil() {
    // PWM Port listed in constants.
    led = new AddressableLED(Constants.led);
    // LED strip length of 50 LEDs. Only set length once.
    ledBuffer = new AddressableLEDBuffer(Constants.bufferSize);
    led.setLength(ledBuffer.getLength());
    // Set data to the LED buffer.
    led.setData(ledBuffer);
    led.start();
  }

  // Creates enumerations for ledStrip, ledColor and ledState.
  public enum LEDStrip {
    LEFT, RIGHT, CENTER, ALL;
  }

  public enum LEDColor {
    RED, YELLOW, GREEN, BLUE, WHITE, OFF;
  }

  public void setAllianceMode() {
    // Gets alliance color and sets r, g and b values to reflect it.
    if (driverStation.getAlliance().equals(Alliance.Red)) {
      setLED(LEDStrip.ALL, LEDColor.RED);
    } else if (driverStation.getAlliance().equals(Alliance.Blue)) {
      setLED(LEDStrip.ALL, LEDColor.BLUE);
    } else {
      setLED(LEDStrip.ALL, LEDColor.OFF);
    }
  }

  public void setSectionColor(LEDStrip strip, int r, int g, int b) {
    // Sets RGB Values of LED strips when passed in the
    // LED strip name (left, center, right, or all), red, green and blue color values.

    int start;
    int end;

    switch(strip) {
      case LEFT:
        start = 0;
        end = Constants.rightOffset;
        break;
      case RIGHT:
        start = Constants.rightOffset;
        end = Constants.centerOffset;
        break;
      case CENTER:
        start = Constants.centerOffset;
        end = ledBuffer.getLength();
        break;
      default:
        start = 0;
        end = ledBuffer.getLength();
        break;
    }

    for (int i = start; i < end; i++) {
      ledBuffer.setRGB(i, r, g, b);
    }

    led.setData(ledBuffer);
  }

  public void setLED(LEDStrip strip, LEDColor color) {
    // Sets color of LED strip to the values of setColor set previously.
    int r;
    int g;
    int b;

    switch(color) {
      case RED:
        r = 255;
        g = 0;
        b = 0;
        break;
      case YELLOW:
        r = 255;
        g = 255;
        b = 0;
        break;
      case GREEN:
        r = 0;
        g = 255;
        b = 0;
        break;
      case BLUE:
        r = 0;
        g = 0;
        b = 255;
        break;
      case WHITE:
        r = 255;
        g = 255;
        b = 255;
        break;
      default:
        r = 0;
        g = 0;
        b = 0;
        break;
    }
    setSectionColor(strip, r, g, b);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
