/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

/**
 * Subsystems
 */
    /**
     * Drive Util
     */
    public static final int leftMaster = 2;
    public static final int leftSlave = 3;
    public static final int rightMaster = 0;
    public static final int rightSlave = 1;

    public static final int leftEncoderForward = 0;
    public static final int leftEncoderReverse = 1;
    public static final int rightEncoderForward = 2;
    public static final int rightEncoderReverse = 3;

    public static final double tickPerRev = 127.0;
    public static final double tickFoot =  80.853;
        /**
         * Coerce to Range values 
         */
        public static final double C2R_inputMin =  -5.0;
        public static final double C2R_inputMax = 5.0;
        public static final double C2R_gyroInputMin = -15.0;
        public static final double C2R_gyroInputMax = 15.0;
        public static final double C2R_outputMin = -0.25;
        public static final double C2R_outputMax = 0.25;
        /**
         * Autonomous command speed values
         */
        public static final double autoDriveSpeed = 0.3;
        public static final double autoStopDistance = 1.5;
        public static final double autoTurnSpeed = 0.2;
        public static final double windowWidth = 0.05;

        public static final int autoShotMotorSpeed = 1;
    /**
     * Collection Util
     */
    public static final int collector = 4;
    public static final double collectorSpeed = 0.5;
    public static final int collectionPistonA = 3;
    public static final int collectionPistonB = 4;
    /**
     * Mag Util
     */
    public static final int belt = 5;
    public static final int beltPiston = 1;
    public static final int bannerSensor = 0;
    /*
     * ShotUtil
     */
    public static final int shotMotor1 = 6;
    public static final int shotMotor2 = 7;
    /**
     * LEDUtil
     */
    public static final int led = 1;
    public static final int bufferSize = 68;
    public static final int leftOffset = 0;
    public static final int rightOffset = 24;
    public static final int centerOffset = 48; // order of wiring from roborio is left, right, center.
    /**
     * PanelUtil
     */
    public static final double spinMotorSpeed = 0.18;
    /**
     * ClimbUtil
     */
    public static final int climbPiston = 2;
    /**
    * Input Mapping
    */
        /**
         * Logitech (DInput) Controller
         */
        //LogiTech F310 Button Mapping X/D Switch = D, Direct Input
        public static final int kLeftXAxisNum = 0;
        public static final int kLeftYAxisNum = 1;
        public static final int kRightXAxisNum = 2;
        public static final int kRightYAxisNum = 3;
        //For DPad, use controller.getPOV();
        public static final int kDPadXAxisNum = 5;
        public static final int kDPadYAxisNum = 6;
        public static final int kXButtonNum = 1;
        public static final int kAButtonNum = 2;
        public static final int kBButtonNum = 3;
        public static final int kYButtonNum = 4;
        public static final int kLeftBumperNum = 5;
        public static final int kRightBumperNum = 6;
        public static final int kLeftTriggerNum = 7;
        public static final int kRightTriggerNum = 8;
        public static final int kBackButtonNum = 9;
        public static final int kStartButtonNum = 10;
        public static final int kLeftStickButtonNum = 11;
        public static final int kRightStickButtonNum = 12;
        /**
        * XInput Controller Mapping (Xbox / Playstation with SCPtoolkit)
        */
        public static final int kOperatorPortNum = 1;
        public static final int kXInputXButtonNum = 3;
        public static final int kXInputAButtonNum = 1;
        public static final int kXInputBButtonNum = 2;
        public static final int kXInputYButtonNum = 4;
        public static final int kXInputLeftBumperNum = 5;
        public static final int kXInputRightBumperNum = 6;
        public static final int kXInputBackButtonNum = 7;
        public static final int kXInputStartButtonNum = 8;
        public static final int kXInputLeftStickButtonNum = 9;
        public static final int kXInputRightStickButtonNum = 10;  
        public static final int kXInputLeftXAxisNum = 0;
        public static final int kXInputRightXAxisNum = 4;
        public static final int kXInputLeftYAxisNum = 1;
        public static final int kXInputRightYAxisNum = 5;
        public static final int kXInputLeftTriggerAxisNum = 2;
        public static final int kXInputRightTriggerAxisNum = 3; 
        /**
         * Joystick Mapping
         */
        public static final int kJoystickButton1 = 1;
        public static final int kJoystickButton2 = 2;
        public static final int kJoystickButton3 = 3;
        public static final int kJoystickButton4 = 4;
        public static final int kJoystickButton5 = 5;
        public static final int kJoystickButton6 = 6;
        public static final int kJoystickButton7 = 7;
        public static final int kJoystickButton8 = 8;
        public static final int kJoystickButton9 = 9;
        public static final int kJoystickButton10 = 10;
        public static final int kJoystickButton11 = 11;
        public static final int kJoystickButton12 = 12;
}
