/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.controlpanel;

import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.PanelUtil;

public class SpinColorWheel extends CommandBase {
  PanelUtil panelUtil;
  private Color initialColor;
  private int countPasses;
  private boolean passedColor;
  /**
   * Creates a new SpinColorWheel.
   */
  public SpinColorWheel(PanelUtil panelUtil) {
    this.panelUtil = panelUtil;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(panelUtil);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    initialColor = panelUtil.getMatchedColor();
    countPasses = 0;
    panelUtil.setCountPasses(countPasses);
    passedColor = false;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    panelUtil.setSpinMotor(0.28);
    if (panelUtil.getMatchedColor().equals(initialColor)) {
      if (passedColor) {
        countPasses++;
        panelUtil.setCountPasses(countPasses);
        passedColor = false;
      }
    }
    else {
      passedColor = true;
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return (countPasses >= 7);
  }
}
